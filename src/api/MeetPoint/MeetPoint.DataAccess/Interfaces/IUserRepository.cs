﻿using MeetPoint.Core.Entities.UserManagement;
using MeetPoint.Core.Interfaces;

namespace MeetPoint.DataAccess.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        public bool GetUserByName(string userName);
    }
}
