﻿using System;
using System.Threading.Tasks;

namespace MeetPoint.DataAccess.Interfaces
{
    public interface IDataHandler : IDisposable
    {
        public IUserRepository Users { get;  }

        public Task SaveChangesAsync();
    }
}
