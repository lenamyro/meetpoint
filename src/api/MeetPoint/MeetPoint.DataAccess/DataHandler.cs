﻿using System;
using System.Threading.Tasks;
using MeetPoint.DataAccess.Interfaces;
using MeetPoint.DataAccess.Repositories;

namespace MeetPoint.DataAccess
{
    /// <summary>
    ///  One endpoint to the database, implementing of Unit of Work pattern.
    /// </summary>
    public class DataHandler : IDataHandler
    {
        private readonly DataContext dataContext;

        private bool disposed;
        private IUserRepository users;

        public DataHandler(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public IUserRepository Users
        {
            get
            {
                if (this.users == null)
                {
                    this.users = new UserRepository(this.dataContext);
                }

                return this.users;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.dataContext.Dispose();
                }
            }

            this.disposed = true;
        }

        public void Dispose()
        {
            this.DisposeDataContext(true);
            GC.SuppressFinalize(this);
        }

        public async Task<int> SaveChangesAsync()
        {
            return await this.dataContext.SaveChangesAsync();
        }

        private void DisposeDataContext(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.dataContext.Dispose();
                }
            }

            this.disposed = true;
        }

        async Task IDataHandler.SaveChangesAsync()
        {
            await this.dataContext.SaveChangesAsync();
        }
    }
}
