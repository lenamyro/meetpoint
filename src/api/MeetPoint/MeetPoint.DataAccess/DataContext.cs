using MeetPoint.Core.Entities.UserManagement;
using Microsoft.EntityFrameworkCore;

namespace MeetPoint.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<UserImage> UserImages { get; set; }

        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // each user has collection of images
            modelBuilder.Entity<UserImage>()
                .HasOne(x => x.User)
                .WithMany(x => x.UserImages)
                .HasForeignKey(x => x.UserId);
        }
    }
}