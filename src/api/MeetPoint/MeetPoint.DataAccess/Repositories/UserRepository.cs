﻿using MeetPoint.Core.Entities.UserManagement;
using MeetPoint.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace MeetPoint.DataAccess.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DataContext dataContext) : base(dataContext)
        {
        }

        // TODO просто пример, удалить его, когда будет база
        public bool GetUserByName(string userName)
        {
            var user = this.DataContext.Set<User>().FirstOrDefaultAsync(x => x.FirstName == userName);
            return user != null;
        }
    }
}
