using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Core.Entities;
using MeetPoint.Core.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace MeetPoint.DataAccess.Repositories
{
    public class Repository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly DataContext dataContext;

        internal Repository(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public DataContext DataContext
        {
            get { return this.dataContext;  }
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await this.dataContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await this.dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<T> CreateAsync(T item)
        {
            var result = await this.dataContext.Set<T>().AddAsync(item);
            return result.Entity;
        }

        public async Task UpdateAsync(T item)
        {
            await this.dataContext.SaveChangesAsync();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await this.GetByIdAsync(id);
            if (item == null)
            {
                return false;
            }

            this.dataContext.Set<T>().Remove(item);
            return true;
        }
    }
}