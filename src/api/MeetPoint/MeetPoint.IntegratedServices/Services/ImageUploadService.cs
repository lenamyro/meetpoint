using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using MeetPoint.IntegratedServices.Interfaces;
using MeetPoint.IntegratedServices.Models;
using MeetPoint.IntegratedServices.Options;
using Microsoft.Extensions.Options;

namespace MeetPoint.IntegratedServices.Services
{
    public class ImageUploadService : IImageUploadService
    {
        private readonly AzureBlobStorageOptions options;

        public ImageUploadService(IOptions<AzureBlobStorageOptions> options)
        {
            this.options = options.Value;
        }

        public async Task UploadImagesAsync(List<ImageUploadModel> images)
        {
            var container = new BlobContainerClient(this.options.ConnectionString, this.options.ContainerName);
            await container.CreateIfNotExistsAsync(PublicAccessType.Blob);

            foreach (var image in images)
            {
                await this.UploadImageAsync(image, container);
            }
        }

        private async Task UploadImageAsync(ImageUploadModel image, BlobContainerClient container)
        {
            var blobName = image.FileName;
            var blob = container.GetBlobClient(blobName);
            var blobUploadOptions = new BlobUploadOptions()
            {
                HttpHeaders = new BlobHttpHeaders
                {
                    ContentType = image.ContentType,
                },
            };

            await blob.UploadAsync(image.File, blobUploadOptions);
        }

        public string BuildUrl(string fileName)
        {
            return Path.Combine(this.options.BaseUrl, this.options.ContainerName, fileName);
        }
    }
}