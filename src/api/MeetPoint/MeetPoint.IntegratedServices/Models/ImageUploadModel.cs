using System.IO;

namespace MeetPoint.IntegratedServices.Models
{
    public class ImageUploadModel
    {
        public Stream File { get; set; }

        public string FileName { get; set; }

        public string ContentType { get; set; }
    }
}