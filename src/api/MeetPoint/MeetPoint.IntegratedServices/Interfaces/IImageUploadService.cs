using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.IntegratedServices.Models;

namespace MeetPoint.IntegratedServices.Interfaces
{
    public interface IImageUploadService
    {
        Task UploadImagesAsync(List<ImageUploadModel> images);

        string BuildUrl(string fileName);
    }
}