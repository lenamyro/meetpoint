using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeetPoint.BusinessLogic.Extensions.PostConfigureActions;
using MeetPoint.BusinessLogic.Interfaces;
using MeetPoint.BusinessLogic.Mappers.InputMappers;
using MeetPoint.BusinessLogic.Mappers.OutputMappers;
using MeetPoint.BusinessLogic.Models.Requests;
using MeetPoint.BusinessLogic.Models.Responses;
using MeetPoint.Core.Exceptions.User;
using MeetPoint.Core.Interfaces;
using MeetPoint.DataAccess.Interfaces;
using MeetPoint.IntegratedServices.Interfaces;

namespace MeetPoint.BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly IDataHandler dataHandler;
        private readonly IImageUploadService imageUploadService;
        private readonly ICurrentDateProvider currentDateProvider;

        public UserService(IDataHandler dataHandler,
            IImageUploadService imageUploadService,
            ICurrentDateProvider currentDateProvider)
        {
            this.dataHandler = dataHandler;
            this.imageUploadService = imageUploadService;
            this.currentDateProvider = currentDateProvider;
        }

        /// <summary> Get all users. </summary>
        public async Task<List<UserResponse>> GetAllUsersAsync()
        {
            var users = (await this.dataHandler.Users.GetAllAsync()).ToList();

            var result = users
                .ToUserResponse()
                .PostConfigureImages(i => i.Url = this.imageUploadService.BuildUrl(i.FileName));

            return result;
        }

        /// <summary> Add a new user. </summary>
        public async Task<UserResponse> AddUserAsync(AddUserRequest request)
        {
            // mappings and post configuring
            var user = request
                .ToUser()
                .PostConfigure(u => u.DateRegistered = this.currentDateProvider.GetCurrentDate())
                .PostConfigureImages(i => i.DatePost = this.currentDateProvider.GetCurrentDate());

            // create user
            var result = await this.dataHandler.Users.CreateAsync(user);

            // upload images to azure blob storage
            var imagesToUpload = request.Images.ToImageUploadModelList();
            await this.imageUploadService.UploadImagesAsync(imagesToUpload);

            // commit transaction
            await this.dataHandler.SaveChangesAsync();

            // prepare response
            var userResponse = result
                .ToUserResponse()
                .PostConfigureImages(image => image.Url = this.imageUploadService.BuildUrl(image.FileName));

            return userResponse;
        }

        public async Task<UserResponse> GetUserByIdAsync(int id)
        {
            // get user by id
            var user = await this.dataHandler.Users.GetByIdAsync(id);

            // TODO throw custom user not found exception

            // prepare response
            var userResponse = user
                .ToUserResponse()
                .PostConfigureImages(image => image.Url = this.imageUploadService.BuildUrl(image.FileName));

            return userResponse;
        }

        public async Task UpdateUserAsync(int userId, UpdateUserRequest request)
        {
            var user = await this.dataHandler.Users.GetByIdAsync(userId);

            if (user == null)
            {
                throw new UserWasNotFoundException();
            }

            user.FirstName = request.FirstName;
            user.MiddleName = request.MiddleName;
            user.LastName = request.LastName;
            user.Phone = request.Phone;
            user.Email = request.Email;
            user.Age = request.Age;

            // TODO handle images for removing

            if (request.NewImages != null)
            {
                // upload new images to azure blob storage
                var imagesToUpload = request.NewImages.ToImageUploadModelList();
                await this.imageUploadService.UploadImagesAsync(imagesToUpload);

                // add new images
                user.UserImages.AddRange(request.NewImages.ToUserImages());
            }

            await this.dataHandler.SaveChangesAsync();
        }

        public async Task<bool> DeleteUserAsync(int userId)
        {
            var isDeleted = await this.dataHandler.Users.DeleteAsync(userId);

            if (!isDeleted)
            {
                return false;
            }

            await this.dataHandler.SaveChangesAsync();
            return true;
        }
    }
}