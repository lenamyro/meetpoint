using System;
using MeetPoint.Core.Interfaces;

namespace MeetPoint.BusinessLogic.Services
{
    public class CurrentDateProvider : ICurrentDateProvider
    {
        public DateTime GetCurrentDate()
        {
            return DateTime.Now;
        }
    }
}