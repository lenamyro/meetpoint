using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace MeetPoint.BusinessLogic.Models.Requests
{
    public class UpdateUserRequest
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public int Age { get; set; }

        public List<int> ImagesForRemoving { get; set; }

        public IFormFileCollection NewImages { get; set; }
    }
}