﻿using Microsoft.AspNetCore.Http;

namespace MeetPoint.BusinessLogic.Models.Requests
{
    public class AddUserRequest
    {
        public string Username { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Password { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public int Age { get; set; }

        public IFormFileCollection Images { get; set; }
    }
}
