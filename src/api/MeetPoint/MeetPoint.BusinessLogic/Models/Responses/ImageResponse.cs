namespace MeetPoint.BusinessLogic.Models.Responses
{
    public class ImageResponse
    {
        public int Id { get; set; }

        public string FileName { get; set; }

        public string Url { get; set; }
    }
}