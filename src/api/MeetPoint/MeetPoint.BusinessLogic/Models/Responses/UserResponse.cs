using System;
using System.Collections.Generic;

namespace MeetPoint.BusinessLogic.Models.Responses
{
    public class UserResponse
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public DateTime DateRegistered { get; set; }

        public string Password { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public int Age { get; set; }

        public List<ImageResponse> Images { get; set; }
    }
}