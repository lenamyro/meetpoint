using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.BusinessLogic.Models.Requests;
using MeetPoint.BusinessLogic.Models.Responses;

namespace MeetPoint.BusinessLogic.Interfaces
{
    public interface IUserService
    {
        Task<List<UserResponse>> GetAllUsersAsync();

        Task<UserResponse> AddUserAsync(AddUserRequest request);

        Task<UserResponse> GetUserByIdAsync(int id);

        Task UpdateUserAsync(int userId, UpdateUserRequest request);

        Task<bool> DeleteUserAsync(int userId);
    }
}