using System;
using System.Collections.Generic;
using MeetPoint.BusinessLogic.Models.Responses;

namespace MeetPoint.BusinessLogic.Extensions.PostConfigureActions
{
    public static class UserResponseExtensions
    {
        public static UserResponse PostConfigureImages(this UserResponse userResponse,
            Action<ImageResponse> configureAction)
        {
            foreach (var image in userResponse.Images)
            {
                configureAction(image);
            }

            return userResponse;
        }

        public static List<UserResponse> PostConfigureImages(this List<UserResponse> userResponses,
            Action<ImageResponse> configureAction)
        {
            foreach (var userResponse in userResponses)
            {
                userResponse.PostConfigureImages(configureAction);
            }

            return userResponses;
        }
    }
}