using System;
using MeetPoint.Core.Entities.UserManagement;

namespace MeetPoint.BusinessLogic.Extensions.PostConfigureActions
{
    public static class UserExtensions
    {
        public static User PostConfigure(this User user,
            Action<User> configureAction)
        {
            configureAction(user);

            return user;
        }

        public static User PostConfigureImages(this User user,
            Action<UserImage> configureAction)
        {
            foreach (var userImage in user.UserImages)
            {
                configureAction(userImage);
            }

            return user;
        }
    }
}