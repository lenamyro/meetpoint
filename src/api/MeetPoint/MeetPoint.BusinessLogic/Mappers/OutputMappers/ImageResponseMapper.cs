using System.Collections.Generic;
using MeetPoint.BusinessLogic.Models.Responses;
using MeetPoint.Core.Entities.UserManagement;

namespace MeetPoint.BusinessLogic.Mappers.OutputMappers
{
    public static class ImageResponseMapper
    {
        public static ImageResponse ToImageResponse(this UserImage userImage)
        {
            return new ImageResponse()
            {
                Id = userImage.Id,
                FileName = userImage.FileName,
            };
        }

        public static List<ImageResponse> ToImageResponse(this List<UserImage> userImages)
        {
            var result = new List<ImageResponse>();

            foreach (var userImage in userImages)
            {
                result.Add(userImage.ToImageResponse());
            }

            return result;
        }
    }
}