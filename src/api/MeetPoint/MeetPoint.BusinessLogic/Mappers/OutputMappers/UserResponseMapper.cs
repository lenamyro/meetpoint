using System.Collections.Generic;
using MeetPoint.BusinessLogic.Models.Responses;
using MeetPoint.Core.Entities.UserManagement;

namespace MeetPoint.BusinessLogic.Mappers.OutputMappers
{
    public static class UserResponseMapper
    {
        public static List<UserResponse> ToUserResponse(this List<User> users)
        {
            var userResponses = new List<UserResponse>();

            foreach (var user in users)
            {
                userResponses.Add(user.ToUserResponse());
            }

            return userResponses;
        }

        public static UserResponse ToUserResponse(this User user)
        {
            return new UserResponse()
            {
                Id = user.Id,
                Username = user.Username,
                FirstName = user.FirstName,
                MiddleName = user.MiddleName,
                LastName = user.LastName,
                DateRegistered = user.DateRegistered,
                Password = user.Password,
                Phone = user.Phone,
                Email = user.Email,
                Age = user.Age,
                Images = user.UserImages?.ToImageResponse(),
            };
        }
    }
}