using System.Collections.Generic;
using MeetPoint.IntegratedServices.Models;
using Microsoft.AspNetCore.Http;

namespace MeetPoint.BusinessLogic.Mappers.InputMappers
{
    public static class ImageUploadModelMapper
    {
        public static List<ImageUploadModel> ToImageUploadModelList(this IFormFileCollection formFileCollection)
        {
            var result = new List<ImageUploadModel>();

            foreach (var formFile in formFileCollection)
            {
                result.Add(formFile.ToImageUploadModel());
            }

            return result;
        }

        public static ImageUploadModel ToImageUploadModel(this IFormFile formFile)
        {
            return new ImageUploadModel()
            {
                FileName = formFile.FileName,
                ContentType = formFile.ContentType,
                File = formFile.OpenReadStream(),
            };
        }
    }
}