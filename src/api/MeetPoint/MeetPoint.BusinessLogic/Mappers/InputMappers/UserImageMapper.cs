using System.Collections.Generic;
using System.IO;
using MeetPoint.Core.Entities.UserManagement;
using Microsoft.AspNetCore.Http;

namespace MeetPoint.BusinessLogic.Mappers.InputMappers
{
    public static class UserImageMapper
    {
        public static List<UserImage> ToUserImages(this IFormFileCollection formFileCollection)
        {
            var result = new List<UserImage>();

            foreach (var formFile in formFileCollection)
            {
                result.Add(formFile.ToUserImage());
            }

            return result;
        }

        public static UserImage ToUserImage(this IFormFile formFile)
        {
            return new UserImage()
            {
                FileName = formFile.FileName,
                Extension = Path.GetExtension(formFile.FileName),
                ContentType = formFile.ContentType,
                SizeByte = formFile.Length,
            };
        }
    }
}