using System;
using MeetPoint.BusinessLogic.Models.Requests;
using MeetPoint.Core.Entities.UserManagement;

namespace MeetPoint.BusinessLogic.Mappers.InputMappers
{
    public static class UserMapper
    {
        public static User ToUser(this AddUserRequest request)
        {
            return new User()
            {
                Username = request.Username,
                FirstName = request.FirstName,
                MiddleName = request.MiddleName,
                LastName = request.LastName,
                DateRegistered = DateTime.Now,
                Password = request.Password,
                Phone = request.Phone,
                Email = request.Email,
                Age = request.Age,
                UserImages = request.Images.ToUserImages(),
            };
        }
    }
}