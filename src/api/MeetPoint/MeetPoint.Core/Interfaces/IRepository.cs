using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.Core.Entities;

namespace MeetPoint.Core.Interfaces
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(int id);

        Task<T> CreateAsync(T item);

        Task UpdateAsync(T item);

        Task<bool> DeleteAsync(int id);
    }
}