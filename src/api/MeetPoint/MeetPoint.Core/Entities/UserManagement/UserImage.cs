using System;

namespace MeetPoint.Core.Entities.UserManagement
{
    public class UserImage : BaseEntity
    {
        public string FileName { get; set; }

        public string Extension { get; set; }

        public string ContentType { get; set; }

        public long SizeByte { get; set; }

        public DateTime DatePost { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}