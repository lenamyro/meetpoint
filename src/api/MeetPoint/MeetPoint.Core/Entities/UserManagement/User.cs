using System;
using System.Collections.Generic;

namespace MeetPoint.Core.Entities.UserManagement
{
    public class User : BaseEntity
    {
        public string Username { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public DateTime DateRegistered { get; set; }

        public string Password { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public int Age { get; set; }

        public virtual List<UserImage> UserImages { get; set; }
    }
}