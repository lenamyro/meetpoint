using MeetPoint.Core.Enums;
using MeetPoint.Core.Resources;

namespace MeetPoint.Core.Exceptions.User
{
    public class UserWasNotFoundException : BaseAppException
    {
        public override int ErrorCode => (int)ErrorCodes.User.UserWasNotFound;

        public UserWasNotFoundException()
            : base(ErrorMessages.UserWasNotFound)
        {
        }

        public UserWasNotFoundException(string message)
            : base(message)
        {
        }
    }
}