using System;
using MeetPoint.Core.Enums;
using MeetPoint.Core.Resources;

namespace MeetPoint.Core.Exceptions
{
    public class BaseAppException : Exception
    {
        public virtual int ErrorCode => (int)ErrorCodes.Global.Unknown;

        public BaseAppException()
            : base(ErrorMessages.Unknown)
        {
        }

        public BaseAppException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}