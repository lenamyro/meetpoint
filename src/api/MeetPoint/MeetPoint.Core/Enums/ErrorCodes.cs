namespace MeetPoint.Core.Enums
{
    public static class ErrorCodes
    {
        // range from 3000 to 3099
        public enum Global
        {
            Unknown = 3000,
        }

        // range from 3100 to 3199
        public enum User
        {
            UserWasNotFound = 3100,
        }
    }
}