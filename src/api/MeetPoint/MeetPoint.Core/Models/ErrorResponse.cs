namespace MeetPoint.Core.Models
{
    public class ErrorResponse
    {
        public int ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        public string ErrorDescription { get; set; }

        public string StackTrace { get; set; }
    }
}