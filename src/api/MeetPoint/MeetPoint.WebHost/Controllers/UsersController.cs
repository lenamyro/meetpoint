using System.Collections.Generic;
using System.Threading.Tasks;
using MeetPoint.BusinessLogic.Interfaces;
using MeetPoint.BusinessLogic.Models.Requests;
using MeetPoint.BusinessLogic.Models.Responses;
using MeetPoint.WebHost.Filters;
using Microsoft.AspNetCore.Mvc;

namespace MeetPoint.WebHost.Controllers
{
    /// <summary> User Management. </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    [ServiceFilter(typeof(AppExceptionFilterAttribute))]
    public class UsersController : Controller
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        /// <summary> Get all users. </summary>
        [HttpGet]
        public async Task<ActionResult<List<UserResponse>>> GetAllUsersAsync()
        {
            var response = await this.userService.GetAllUsersAsync();

            return this.Ok(response);
        }

        /// <summary> Get user by id. </summary>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<UserResponse>> GetUserByIdAsync(int id)
        {
            var response = await this.userService.GetUserByIdAsync(id);

            return this.Ok(response);
        }

        /// <summary> Add a new user. </summary>
        [HttpPost]
        public async Task<ActionResult<UserResponse>> AddUserAsync([FromForm] AddUserRequest request)
        {
            var response = await this.userService.AddUserAsync(request);

            return this.Ok(response);
        }

        /// <summary> Update user. </summary>
        [HttpPut]
        public async Task<IActionResult> UpdateUserAsync(int userId, [FromForm] UpdateUserRequest request)
        {
            await this.userService.UpdateUserAsync(userId, request);

            return this.StatusCode(200);
        }

        /// <summary> Delete user. </summary>
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> DeleteUserAsync(int id)
        {
            if (await this.userService.DeleteUserAsync(id))
            {
                return this.StatusCode(204);
            }

            return this.NotFound();
        }
    }
}