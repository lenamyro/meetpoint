namespace MeetPoint.WebHost.Options
{
    public class DatabaseOptions
    {
        public string ConnectionString { get; set; }
    }
}