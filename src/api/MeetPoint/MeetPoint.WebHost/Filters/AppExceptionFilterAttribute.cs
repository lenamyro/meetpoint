using System;
using MeetPoint.Core.Enums;
using MeetPoint.Core.Exceptions;
using MeetPoint.Core.Models;
using MeetPoint.Core.Resources;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace MeetPoint.WebHost.Filters
{
    public class AppExceptionFilterAttribute : Attribute, IExceptionFilter
    {
        private readonly ILogger<AppExceptionFilterAttribute> logger;
        private readonly IWebHostEnvironment hostEnvironment;

        public AppExceptionFilterAttribute(ILogger<AppExceptionFilterAttribute> logger,
            IWebHostEnvironment hostEnvironment)
        {
            this.logger = logger;
            this.hostEnvironment = hostEnvironment;
        }

        public void OnException(ExceptionContext context)
        {
            var isDevelopment = this.hostEnvironment.IsDevelopment();
            var exception = context.Exception;
            var errorResponse = new ErrorResponse();
            int statusCode;

            if (context.Exception.GetType().IsSubclassOf(typeof(BaseAppException)))
            {
                errorResponse.ErrorCode = (exception as BaseAppException).ErrorCode;
                errorResponse.ErrorMessage = exception.Message;

                statusCode = StatusCodes.Status400BadRequest;
            }
            else
            {
                this.logger.LogError(exception, exception.Message);

                errorResponse.ErrorCode = (int)ErrorCodes.Global.Unknown;

                if (isDevelopment)
                {
                    errorResponse.StackTrace = exception.StackTrace;
                    errorResponse.ErrorMessage = exception.Message;
                }
                else
                {
                    errorResponse.ErrorMessage = ErrorMessages.Unknown;
                }

                statusCode = StatusCodes.Status500InternalServerError;
            }

            context.Result = new ObjectResult(errorResponse) { StatusCode = statusCode };
            context.ExceptionHandled = true;
        }
    }
}