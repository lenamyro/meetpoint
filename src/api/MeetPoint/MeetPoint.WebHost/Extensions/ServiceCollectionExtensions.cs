using MeetPoint.BusinessLogic.Interfaces;
using MeetPoint.BusinessLogic.Services;
using MeetPoint.Core.Constants;
using MeetPoint.Core.Interfaces;
using MeetPoint.DataAccess;
using MeetPoint.DataAccess.Interfaces;
using MeetPoint.DataAccess.Repositories;
using MeetPoint.IntegratedServices.Interfaces;
using MeetPoint.IntegratedServices.Options;
using MeetPoint.IntegratedServices.Services;
using MeetPoint.WebHost.Filters;
using MeetPoint.WebHost.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace MeetPoint.WebHost.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDataAccessServices(this IServiceCollection services)
        {
            services.AddDbContext<DataContext>((provider, builder) =>
            {
                var options = provider.GetRequiredService<IOptions<DatabaseOptions>>().Value;

                builder.UseSqlServer(options.ConnectionString);
                builder.UseLazyLoadingProxies();
            });

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IDataHandler, DataHandler>();
        }

        public static void AddBisunessLogicServices(this IServiceCollection services)
        {
            services.AddSingleton<ICurrentDateProvider, CurrentDateProvider>();

            services.AddScoped<IUserService, UserService>();
        }

        public static void AddIntegratedServices(this IServiceCollection services)
        {
            services.AddScoped<IImageUploadService, ImageUploadService>();
        }

        public static void AddAppConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionStringDefaultName = AppSettingsConstants.ConnectionStrings.DefaultConnection;
            var connectionStringDefault = configuration.GetConnectionString(connectionStringDefaultName);

            services.Configure<DatabaseOptions>(x => x.ConnectionString = connectionStringDefault);
            services.Configure<AzureBlobStorageOptions>(configuration.GetSection(AzureBlobStorageOptions.Position));
        }

        public static void AddOpenApiDocument(this IServiceCollection services)
        {
            services.AddOpenApiDocument(document =>
            {
                document.Title = "MeetPoint API Doc";
                document.Version = "1.0";
            });
        }

        public static void AddFilters(this IServiceCollection services)
        {
            services.AddScoped<AppExceptionFilterAttribute>();
        }
    }
}